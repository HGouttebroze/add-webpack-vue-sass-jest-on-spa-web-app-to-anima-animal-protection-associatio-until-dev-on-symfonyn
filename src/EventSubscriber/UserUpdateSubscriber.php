<?php

namespace App\EventSubscriber;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserUpdateSubscriber implements EventSubscriberInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

/* THIS FUNCTION IS CREATED IN ORDER TO ENCODE PASSWORD DIRECTLY FROM DASHBOARD WHEN CREATE OR UPDATE THIS */
    public function onBeforeEntityPersistedEvent($event)
    {
        // dd($event); // debug

        $user = $event->getEntityInstance();

        if (!$user instanceof User) {
            return;
        }

        if (empty($user->getPlainPassword())) {
            return;
        }
        $newPwd = $this->encoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($newPwd);
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => 'onBeforeEntityPersistedEvent',
            BeforeEntityUpdatedEvent::class => 'onBeforeEntityPersistedEvent',
        ];
    }
}
