<?php

namespace App\Entity;

use App\Repository\AssociationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AssociationRepository", repositoryClass=AssociationRepository::class)
 */
class Association
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $announcementsProvided;


    /**
     * @ORM\Column(type="text")
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=Dog::class, mappedBy="association")
     */
    private $Dog;

    /**
     * @ORM\OneToMany(targetEntity=Adoption::class, mappedBy="association")
     */
    private $Adoption;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="association")
     */
    private $User;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=ContactUser::class, mappedBy="Association")
     */
    private $contactUsers;

    public function __construct()
    {
        $this->Dog = new ArrayCollection();
        $this->Adoption = new ArrayCollection();
        $this->User = new ArrayCollection();
        $this->contactUsers = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }



    public function getAnnouncementsProvided(): ?int
    {
        return $this->announcementsProvided;
    }

    public function setAnnouncementsProvided(?int $announcementsProvided): self
    {
        $this->announcementsProvided = $announcementsProvided;

        return $this;
    }


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Dog[]
     */
    public function getDog(): Collection
    {
        return $this->Dog;
    }

    public function addDog(Dog $dog): self
    {
        if (!$this->Dog->contains($dog)) {
            $this->Dog[] = $dog;
            $dog->setAssociation($this);
        }

        return $this;
    }

    public function removeDog(Dog $dog): self
    {
        if ($this->Dog->removeElement($dog)) {
            // set the owning side to null (unless already changed)
            if ($dog->getAssociation() === $this) {
                $dog->setAssociation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Adoption[]
     */
    public function getAdoption(): Collection
    {
        return $this->Adoption;
    }

    public function addAdoption(Adoption $adoption): self
    {
        if (!$this->Adoption->contains($adoption)) {
            $this->Adoption[] = $adoption;
            $adoption->setAssociation($this);
        }

        return $this;
    }

    public function removeAdoption(Adoption $adoption): self
    {
        if ($this->Adoption->removeElement($adoption)) {
            // set the owning side to null (unless already changed)
            if ($adoption->getAssociation() === $this) {
                $adoption->setAssociation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->User;
    }

    public function addUser(User $user): self
    {
        if (!$this->User->contains($user)) {
            $this->User[] = $user;
            $user->setAssociation($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->User->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getAssociation() === $this) {
                $user->setAssociation(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ContactUser[]
     */
    public function getContactUsers(): Collection
    {
        return $this->contactUsers;
    }

    public function addContactUser(ContactUser $contactUser): self
    {
        if (!$this->contactUsers->contains($contactUser)) {
            $this->contactUsers[] = $contactUser;
            $contactUser->setAssociation($this);
        }

        return $this;
    }

    public function removeContactUser(ContactUser $contactUser): self
    {
        if ($this->contactUsers->removeElement($contactUser)) {
            // set the owning side to null (unless already changed)
            if ($contactUser->getAssociation() === $this) {
                $contactUser->setAssociation(null);
            }
        }

        return $this;
    }


}
