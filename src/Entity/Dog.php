<?php

namespace App\Entity;

use App\Entity\Traits\HasIdTrait;
use App\Entity\Traits\HasNameTrait;
use App\Repository\DogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DogRepository::class)
 */
class Dog
{
    use HasNameTrait;
    use HasIdTrait;


    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Adoption::class,
     *     inversedBy="dogs", cascade={"persist"})
     */
    private $adoption;

    /**
     * @ORM\ManyToMany(targetEntity=Breed::class, mappedBy="dogs")
     */
    private $breeds;



    /**
     * @ORM\Column(type="boolean")
     */
    private $taken;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="dogs")
     */
    private $user;

    /**
     * @ORM\Column(type="text")
     */
    private $history;

    /**
     * @ORM\Column(type="boolean")
     */
    private $LOF;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sociabilityToAnimals;

    /**
     * @ORM\ManyToOne(targetEntity=Association::class, inversedBy="Dog")
     */
    private $association;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Picture", mappedBy="dog", orphanRemoval=true, cascade={"persist"})
     */
    private $pictures;

    /**
     * @Assert\All({
     *   @Assert\Image(mimeTypes="image/jpeg")
     * })
     */
    private $pictureFiles;


    public function __construct()
    {
        $this->breeds = new ArrayCollection();
        $this->pictures = new ArrayCollection();
        $this->LOF = false;
        $this->sociabilityToAnimals = false;
        $this->taken = false;
        $this->sociabilityToAnimals = false;
        $this->LOF = false;
    }


    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAdoption(): ?Adoption
    {
        return $this->adoption;
    }

    public function setAdoption(?Adoption $adoption): self
    {
        $this->adoption = $adoption;

        return $this;
    }

    /**
     * @return Collection|Breed[]
     */
    public function getBreeds(): Collection
    {
        return $this->breeds;
    }

    public function addBreed(Breed $breed): self
    {
        if (!$this->breeds->contains($breed)) {
            $this->breeds[] = $breed;
            $breed->addDog($this);
        }

        return $this;
    }

    public function removeBreed(Breed $breed): self
    {
        if ($this->breeds->removeElement($breed)) {
            $breed->removeDog($this);
        }

        return $this;
    }

/* add dog's age */
    public function getAge(): ?int
    {
        if (empty($this->getBirthDate())) {
            return null;
        }

        $now = new DateTime();

        $diff = $this->getBirthDate()->diff($now, true);

        return $diff->y;
    }

    public function getTaken(): ?bool
    {
        return $this->taken;
    }

    public function setTaken(bool $taken): self
    {
        $this->taken = $taken;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getHistory(): ?string
    {
        return $this->history;
    }

    public function setHistory(string $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getLOF(): ?bool
    {
        return $this->LOF;
    }

    public function setLOF(bool $LOF): self
    {
        $this->LOF = $LOF;

        return $this;
    }

    public function getSociabilityToAnimals(): ?bool
    {
        return $this->sociabilityToAnimals;
    }

    public function setSociabilityToAnimals(bool $sociabilityToAnimals): self
    {
        $this->sociabilityToAnimals = $sociabilityToAnimals;

        return $this;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): self
    {
        $this->association = $association;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function getPicture(): ?Picture
    {
        if ($this->pictures->isEmpty()) {
            return null;
        }
        return $this->pictures->first();
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setDog($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->contains($picture)) {
            $this->pictures->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getDog() === $this) {
                $picture->setDog(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPictureFiles()
    {
        return $this->pictureFiles;
    }

    /**
     * @param mixed $pictureFiles
     * @return Dog
     */
    public function setPictureFiles($pictureFiles): self
    {
        foreach($pictureFiles as $pictureFile) {
            $picture = new Picture();
            $picture->setImageFile($pictureFile);
            $this->addPicture($picture);
        }
        $this->pictureFiles = $pictureFiles;
        return $this;
    }



}
