<?php

namespace App\Entity;

use App\Entity\Traits\HasIdTrait;
use App\Repository\PictureRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=PictureRepository::class)
 * @Vich\Uploadable()
 */
class Picture
{

    use TimestampableEntity;
    use HasIdTrait;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;           // like path name

    /**
     * @var File|null
     * @Assert\Image(
     *     mimeTypes="image/jpeg"
     * )
     * @Vich\UploadableField(mapping="products", fileNameProperty="filename")
     */
    private $imageFile;



    /**
     * @ORM\ManyToOne(targetEntity=Cat::class, inversedBy="pictures")
     * @ORM\JoinColumn(nullable=true)
     */
    private $cat;

    /**
     * @return mixed
     */
    public function getCat(): ?Cat
    {
        return $this->cat;
    }

    public function setCat(?Cat $cat): self
    {
        $this->cat = $cat;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity=Dog::class, inversedBy="pictures")
     * @ORM\JoinColumn(nullable=true)
     */
    private $dog;

    /**
     * @return mixed
     */
    public function getDog()
    {
        return $this->dog;
    }

    /**
     * @param mixed $dog
     * @return Picture
     */
    public function setDog($dog)
    {
        $this->dog = $dog;
        return $this;
    }


    /**
     * @ORM\ManyToOne(targetEntity=Adoption::class, inversedBy="pictures")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adoption;

    /**
     * @ORM\ManyToOne(targetEntity=Media::class, inversedBy="pictures")
     */
    private $media;

    /**
     * @return mixed
     */
    public function getAdoption()
    {
        return $this->adoption;
    }

    /**
     * @param mixed $adoption
     */
    public function setAdoption($adoption): void
    {
        $this->adoption = $adoption;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }



    /**
     * @return null|File
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param null|File $imageFile
     * @return self
     */
    public function setImageFile(?File $imageFile): self
    {

        $this->imageFile = $imageFile;

        // force to change
        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this;
    }

   public function __toString(): string
    {
        return (string) $this->getFilename();
    }

   public function getMedia(): ?Media
   {
       return $this->media;
   }

   public function setMedia(?Media $media): self
   {
       $this->media = $media;

       return $this;
   }

}
