<?php

namespace App\Form;

use App\Entity\Adoption;
use App\Entity\Cat;
use App\Entity\Dog;
use App\Entity\Picture;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('imageFile', VichImageType::class, [
            'required' => true,
            'allow_delete' => true,
            'delete_label' => 'Supprimer',
            'download_label' => 'Télécharger',
            'download_uri' => true,
            'image_uri' => true,
            //'imagine_pattern' => '...',
            // if use 'imagine' bundle
            'asset_helper' => true,
            //'by_reference' => false,
            ])
            /*
             * ->add('filename')
            ->add('createdAt')
            ->add('updatedAt')
            */
            ->add('cat', EntityType::class, [
                'class' => Cat::class,
                'required' => false,
            ])
            ->add('dog', EntityType::class, [
                'class' => Dog::class,
                'required' => false,
            ])
            ->add('adoption', EntityType::class, [
                'class' => Adoption::class,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
