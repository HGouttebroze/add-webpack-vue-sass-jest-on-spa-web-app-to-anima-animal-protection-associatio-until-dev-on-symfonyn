<?php

namespace App\Controller\Admin;

use App\Entity\Picture;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichFileType;

class PictureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Picture::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('imageFile')->setFormType(VichFileType::class)->onlyWhenCreating(),
            ImageField::new('filename')->setBasePath('/images/dogs')->onlyOnIndex(),
            AssociationField::new('adoption'),
            AssociationField::new('dog')->setRequired(false)->setDisabled(true),
            AssociationField::new('cat'),
            AssociationField::new('media')
        ];
    }

}
