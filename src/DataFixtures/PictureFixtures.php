<?php


namespace App\DataFixtures;



use App\Entity\Picture;
use App\DataFixtures\CatFixtures;
use App\DataFixtures\DogFixtures;
use App\Repository\CatRepository;
use App\Repository\DogRepository;
use App\Repository\AdoptionRepository;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{


    public function __construct(AdoptionRepository $adoptionRepository, DogRepository $dogRepository, CatRepository $catRepository)
    {
        $this->adoptionRepository = $adoptionRepository;
        $this->dogRepository = $dogRepository;
        $this->catRepository  = $catRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $date = new DateTime('2021-06-06');
        $date1 = new DateTime('2021-07-06');
        $date2 = new DateTime('2021-08-06');
        $date3 = new DateTime('2021-09-06');

        $cats = $this->catRepository->findAll();
        $dogs = $this->dogRepository->findAll();

        for ($i = 1; $i < 16; $i++) {
            $catPicture = new Picture();
            $catPicture->setFilename('https://images.unsplash.com/photo-1508814437933-f0c7d18a9217?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzR8fGRvZ3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');
            $randomNumber1 = mt_rand(0, count($cats) - 1);
            $catPicture->setCat($cats[$randomNumber1]);
            $catPicture->setUpdatedAt($date);
            $catPicture->setCreatedAt($date);
            $manager->persist($catPicture);
        }

        for ($i = 1; $i < 16; $i++) {
            $dogPicture = new Picture();
            $dogPicture->setFilename('https://images.unsplash.com/photo-1508814437933-f0c7d18a9217?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzR8fGRvZ3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');
            $randomNumber2 = mt_rand(0, count($dogs) - 1);
            $dogPicture->setDog($dogs[$randomNumber2]);
            $dogPicture->setUpdatedAt($date1);
            $dogPicture->setCreatedAt($date1);
            $manager->persist($dogPicture);
        }


        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            DogFixtures::class,
            CatFixtures::class
        ];
    }
}
