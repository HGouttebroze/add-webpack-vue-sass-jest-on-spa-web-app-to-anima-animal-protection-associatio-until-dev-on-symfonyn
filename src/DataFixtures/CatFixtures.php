<?php


namespace App\DataFixtures;


use App\Entity\Cat;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CatFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $date = new DateTime('2021-06-06');
        $date1 = new DateTime('2021-07-06');
        $date2 = new DateTime('2021-08-06');
        $date3 = new DateTime('2021-09-06');

        // Create dummy datas to create a data base unit test (SQLite DB)
        // (I create 18 cats, so I must find this number when testing)
        for ($i = 1; $i < 19; $i++) {
            $cats = new Cat();
            $cats->setName('Chats n°' . $i);
            $cats->setDescription('Miaou, je suis le chat n°' . $i . ', je ronronne beaucoup quand on me carresse et je 
            fait "MiaouMiaou". Je n\ai qu\'un numéro, le ' . $i . ', donnez moi un nom sympa... Adoptez moi! Miaou!');
            $cats->setPictureFiles('followYourDreams.jpg');
            $cats->setUpdatedAt($date);
            $cats->setCreatedAt($date);
            $manager->persist($cats);
        }
        $manager->flush();
    }
}
