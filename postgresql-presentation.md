# Téléchargements Linux (Ubuntu) Logo Ubuntu

PostgreSQL est disponible dans toutes les versions d'Ubuntu par défaut. Cependant, Ubuntu "snapshots" une version spécifique de PostgreSQL qui est ensuite prise en charge pendant toute la durée de vie de cette version d'Ubuntu. D'autres versions de PostgreSQL sont disponibles via le référentiel PostgreSQL apt.

Référentiel PostgreSQL Apt
Si la version incluse dans votre version d'Ubuntu n'est pas celle que vous souhaitez, vous pouvez utiliser le PostgreSQL Apt Repository . Ce référentiel s'intégrera à vos systèmes normaux et à la gestion des correctifs, et fournira des mises à jour automatiques pour toutes les versions prises en charge de PostgreSQL tout au long de la durée de vie du support de PostgreSQL.

Le référentiel PostgreSQL Apt prend en charge les versions actuelles d'Ubuntu :

espiègle (21.10)
hirsute (21,04)
focale (20.04)
bionique (18.04)
sur les architectures suivantes :

amd64
arm64 (18.04 et versions ultérieures ; versions LTS uniquement)
i386 (18.04 et plus)
ppc64el (versions LTS uniquement)
Pour utiliser le référentiel apt, procédez comme suit :

# Créez la configuration du référentiel de fichiers :

sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Importez la clé de signature du référentiel :

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key ajouter -

# Mettre à jour les listes de paquets :

sudo apt-get mise à jour

# Installez la dernière version de PostgreSQL.

# Si vous voulez une version spécifique, utilisez 'postgresql-12' ou similaire au lieu de 'postgresql' :

sudo apt-get -y installer postgresql
Copier le script
Pour plus d'informations sur le référentiel apt, y compris les réponses aux questions fréquentes, veuillez consulter la page PostgreSQL Apt Repository sur le wiki .

Inclus dans la diffusion
Ubuntu inclut PostgreSQL par défaut. Pour installer PostgreSQL sur Ubuntu, utilisez la commande apt-get (ou autre apt-driving) :

apt-get install postgresql-12
Le référentiel contient de nombreux packages différents, y compris des modules complémentaires tiers. Les packages les plus courants et les plus importants sont (remplacez le numéro de version si nécessaire) :

postgresql-client-12 bibliothèques clientes et binaires client
postgresql-12 serveur de base de données principal
postgresql-contrib-9.x modules supplémentaires fournis (partie du package postgresql-xx dans les versions 10 et ultérieures)
libpq-dev bibliothèques et en-têtes pour le développement frontal en langage C
postgresql-server-dev-12 bibliothèques et en-têtes pour le développement backend en langage C
pgadmin4 Utilitaire d'administration graphique pgAdmin 4
