<?php


namespace App\Tests;


use App\Repository\CatRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CatRepositoryTest extends KernelTestCase
{
    /*
        test de BDD: je veux vérifier si le nombre de ....
        que je suposse avoir en base correspond à
        celui trouvé par le kernel de Symfony,
        si le nombre est égal, le test passe et aura le status SUCCESS, sinon, il échoue
    */

    public function testCount(){
        self::bootKernel(); // j recup le noyau
        $cats = self::$container->get(CatRepository::class)->count([]); // j'utilise la fonction count()
        // j'ai mon repo déjà configuré et j'appel method count avec aucun critere, sensé m donné 1 nombre de chat en bbd
        $this->assertEquals(18, $cats); // pour que le test passe,
    }
}
